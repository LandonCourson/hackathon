<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = [];

    public function education()
    {
        return $this->belongsToMany('App\Education', 'education_profile', 'profile_id', 'education_id');
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill', 'skills_profile', 'profile_id', 'skill_id');
    }

    public function interest()
    {
        return $this->belongsToMany('App\Interest', 'interest_profile', 'profile_id', 'interest_id');
    }
}
