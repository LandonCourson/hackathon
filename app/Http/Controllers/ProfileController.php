<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        $user = Auth::user()->id;

        $profile_test = Profile::where('user_id', '=', $user)
            ->first();

        //dd($profile);

        if(empty($profile_test))
        {
            $pro = new Profile();
            $pro->user_id = $user;
            $pro->save();

            $profile = Profile::where('user_id', '=', $user)
                ->first();

            return view('profile.index', compact('profile'));

        }else{


            $profile = Profile::where('user_id', '=', $user)
                ->first();

            return view('profile.index', compact('profile'));
        }



    }
}
