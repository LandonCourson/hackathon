<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile;
use App\Skill;
use Illuminate\Http\Request;

class SkillsController extends Controller
{
    public function index()
    {
        $skills = Skill::orderBy('skills')->get();

        return view('skill.index', compact('skills'));
    }

    public function create(Request $request)
    {
        $profile = Profile::where('user_id', '=', Auth::user()->id)->first();

        $profile->skills()->attach($request->skills);

        flash()->success('Success!', 'You have added skills to your profile!');
        return redirect()->route('profile', $profile);
        
    }
}
