<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile;
use App\Interest;
use Illuminate\Http\Request;

class InterestController extends Controller
{
    public function index()
    {
        $interest = Interest::orderBy('interest')->get();

        return view('interest.index', compact('interest'));
    }

    public function create(Request $request)
    {
        $profile = Profile::where('user_id', '=', Auth::user()->id)->first();

        $profile->interest()->attach($request->interest);

        flash()->success('Success!', 'You have added interests to your profile!');
        return redirect()->route('profile', $profile);
    }
}
