<?php

namespace App\Http\Controllers;

use Auth;
use App\Profile;
use App\Education;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    public function index(){

        $education = Education::orderby('subject')
            ->get();
        //dd($education);

        return view('education.index',compact('education'));
    }

    public function create(Request $request)
    {
        $profile = Profile::where('user_id', '=', Auth::user()->id)->first();

        $profile->education()->attach($request->education);

        flash()->success('Success!', 'You have added Education experience to your profile!');
        return redirect()->route('profile', $profile);
    }
}
