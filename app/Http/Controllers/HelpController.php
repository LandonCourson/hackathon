<?php

namespace App\Http\Controllers;

use App\proint;
use App\proskill;
use App\proedu;
use App\Education;
use App\Skill;
use App\Interest;
use Illuminate\Http\Request;

class HelpController extends Controller
{
    public function index()
    {
        $education = Education::orderby('subject')->get();

        $interest = Interest::orderBy('interest')->get();

        $skills = Skill::orderBy('skills')->get();

        return view('help.index', compact('education', 'interest', 'skills'));
    }

    public function post(Request $request)
    {

        $education = Education::where('id', '=', $request->edu)->get();
        $skills = Skill::where('id', '=', $request->skill)->get();
        $interest = Interest::where('id', '=', $request->int)->get();
dd($education, $skills, $interest);
        $proint = proint::where('interest_id', '=', $interest)->get();
        $proedu = proedu::where('education_id', '=', $education)->get();
        $proskill = proskill::where('skill_id', '=', $skills)->get();

        dd($proint, $proedu, $proskill);
    }
}
