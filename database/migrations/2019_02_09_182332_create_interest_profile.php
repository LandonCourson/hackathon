<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('interest_id')->unsigned();
        });

        Schema::table('interest_profile', function($table) {
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('interest_id')->references('id')->on('interests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_profile');
    }
}
