<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('education_id')->unsigned();

        });

        Schema::table('education_profile', function($table) {
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('education_id')->references('id')->on('education');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_profile');
    }
}
