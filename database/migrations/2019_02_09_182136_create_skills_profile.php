<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkillsProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skills_profile', function (Blueprint $table) {
            $table->integer('profile_id')->unsigned();
            $table->integer('skill_id')->unsigned();
        });

        Schema::table('skills_profile', function($table) {
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->foreign('skill_id')->references('id')->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skills_profile');
    }
}
