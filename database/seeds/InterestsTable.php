<?php

use Illuminate\Database\Seeder;

class InterestsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests')->insert(
            array(
                array('interest' => 'Acting Drama', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Amateur astronomy', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Amateur radio', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Archery', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Backgammon', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Basketball', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Blogging', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Board games', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Bodybuilding', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Calligraphy', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Car restoration', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Chess', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Childcare ', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Club leadership', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Coding Programming', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Collecting', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Cooking Cooking', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Crafts', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Creative writing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Cricket', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Crossword puzzles', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Cycling', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Dancing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Drawing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Fishing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Football', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Gambling', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Gardening', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Genealogy', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Golf', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Hiking', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Hunting', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Investing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Jigsaw puzzles', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Knitting', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Landscaping', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Languages', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Lego building', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Marathon running', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Martial arts', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Meditation', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Model building', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Mountain biking', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Mountain climbing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Musical instrument', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Online classes', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Origami', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Painting', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Papermaking', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Parachuting', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Photography', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Pottery', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Reading', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Recycling', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Robotics', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Rock climbing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Running Jogging', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Sailing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Singing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Skiing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Skydiving', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Socialising', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Squash', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Surfing', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Swimming', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Tennis', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Travelling', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Video games', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Video production', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Volunteering', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Woodworking', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('interest' => 'Yoga', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            ));
    }
}
