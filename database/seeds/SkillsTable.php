<?php

use Illuminate\Database\Seeder;

class SkillsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert(
            array(
                array('skills' => 'Accounting', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Computer Programming', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Foreign Languages', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Drivers License', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Bookkeeping', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Data Analysis', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Mathematics', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Project Management', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Teaching', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Engineering', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Communication', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Flexibility', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Integrity', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Teamwork', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Professionalism', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Interpersonal Skills', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Responsibility', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Positive attitude', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Problem Solving', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Initiative', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Leadership', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Continuous Learning', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Adaptability', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Self-motivation', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Self-management', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Written and Verbal Communication', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Computer Skills', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Delegation', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Planning', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
                array('skills' => 'Responsibility', 'created_at' => Carbon\Carbon::now(), 'updated_at' => \Carbon\Carbon::now()),
            ));
    }
}
