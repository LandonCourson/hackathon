<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/profile', ['as' => 'profile', 'uses' => 'ProfileController@index']);

    Route::get('/education', 'EducationController@index');
    Route::POST('/education/create', 'EducationController@create');

    Route::get('/interest', 'InterestController@index');
    Route::POST('/interest/create', 'InterestController@create');

    Route::get('/skills', 'SkillsController@index');
    Route::POST('/skills/create', 'SkillsController@create');

    Route::get('/help', 'HelpController@index');
    Route::POST('/help/post', 'HelpController@post');

});
