
@extends('layouts.app')


@section('content')



    <div class="card" style="background-color: #7742f4">
        <div class="card-header text-white">
            {{ Auth::user()->Fname }} {{ Auth::user()->Lname }}
        </div>
        <div class="card-body">

            <div class="card-deck">

                <div class="card">
                    <div class="card-header">
                        Skills
                    </div>
                    <div class="card-body">

                        <form action="/skills/create" method="POST">
                            @csrf
                            <select class="js-example-basic-multiple form-control" name="skills[]" multiple="multiple">
                                @foreach($skills as $skill)
                                    <option value="{{$skill->id}}">{{$skill->skills}}</option>
                                @endforeach
                            </select>

                            <input type="submit" class="btn btn-primary btn-block mt-4">
                        </form>

                    </div>
                </div>

            </div>

        </div>
    </div>



@endsection
@section('scripts')


    <script>
        $(document).ready(function() {
            $(".js-example-basic-multiple").select2();
        });
    </script>



@endsection


