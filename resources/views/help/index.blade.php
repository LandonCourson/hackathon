@extends('layouts.app')


@section('content')

<div class="card">
    <div class="card-header text-white"  style="background-color: #7742f4">
        Request for Assistance
    </div>
    <div class="card-body">
        <form action="/help/post" method="POST">
            @csrf

            <label for="cat">Categories</label>
            <select class="js-example-basic-multiple form-control" name="edu[]" multiple="multiple">
                
                @foreach($education as $edu)
                    <option value="{{$edu->id}}">{{$edu->subject}}</option>
                @endforeach
               
            </select>

            <br><br>
            <label for="cat">Skills</label>
            <select class="js-example-basic-multiple form-control" name="skill[]" multiple="multiple">
                
                @foreach($skills as $skill)
                    <option value="{{$skill->id}}">{{$skill->skills}}</option>
                @endforeach
               
            </select>

            <br><br>
            <label for="cat">Interests</label>
            <select class="js-example-basic-multiple form-control" name="int[]" multiple="multiple">
                
                @foreach($interest as $int)
                    <option value="{{$int->id}}">{{$int->interest}}</option>
                @endforeach
               
            </select>

            <input type="submit" class="btn btn-primary btn-block mt-4">
            
        </form>
    </div>
</div>

@endsection


@section('scripts')


    <script>
        $(document).ready(function() {
            $(".js-example-basic-multiple").select2();
        });
    </script>



@endsection