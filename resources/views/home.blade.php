@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <div class="row">
                            <div class="col-lg-6">
                                <a href="/help" class="btn btn-primary btn-block">Recieve Help</a>
                            </div>
                            <div class="col-lg-6">
                                <a href="/profile" class="btn btn-success btn-block">Help Others</a>
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
