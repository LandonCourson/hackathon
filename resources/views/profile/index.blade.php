@extends('layouts.app')


@section('content')


<div class="card" style="background-color: #7742f4">
    <div class="card-header text-white">
        {{ Auth::user()->Fname }} {{ Auth::user()->Lname }}
    </div>
    <div class="card-body">
        
        <div class="card-deck">

            <div class="card">
                <div class="card-header">
                  <i class="fas fa-user-graduate"></i> Education <i class="fas fa-exclamation-triangle float-right"></i>
                </div>
                <div class="card-body">

                    @forelse($profile->education as $edu)
                        <li>{{$edu->subject}}</li>
                    @empty
                        <div class="alert alert-warning">You have not added any education experience to your profile!</div>
                    @endforelse

                </div>
                <div class="card-footer">
                    <a href="/education" class="mt-2 btn btn-block btn-primary">Add Education</a>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Skills
                </div>
                <div class="card-body">
                    
                    @forelse($profile->skills as $skill)
                        <li>{{$skill->skills}}</li>
                    @empty
                        <div class="alert alert-warning">You have not added any skills to your profile!</div>
                    @endforelse
                    
                </div>
                <div class="card-footer">
                    <a href="/skills" class="mt-2 btn btn-block btn-primary">Add Skills</a>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Interest
                </div>
                <div class="card-body">
                    
                    @forelse($profile->interest as $int)
                        <li>{{$int->interest}}</li>
                    @empty
                        <div class="alert alert-warning">You have not added any interests to your profile!</div>
                    @endforelse
                    
                </div>
                <div class="card-footer">
                    <a href="/interest" class="mt-2 btn btn-block btn-primary">Add Interest</a>
                </div>
            </div>

        </div>

    </div>
</div>


@endsection